package demo.pages;

import demo.webdriver.WebPageDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class HomePage {

    public void openHome(){
        WebPageDriver.webDriver.get("https://bulbapedia.bulbagarden.net/wiki/Main_Page");
    }

    public void search(String keyword){
        WebElement inputSearch = WebPageDriver.webDriver.findElement(By.xpath("//input[@id='searchInput']"));
        inputSearch.sendKeys(keyword);
    }

    public void enterKey(){
        WebElement pressKey = WebPageDriver.webDriver.findElement(By.xpath("//input[@id='searchInput']"));
        pressKey.sendKeys(Keys.ENTER);
    }
}
