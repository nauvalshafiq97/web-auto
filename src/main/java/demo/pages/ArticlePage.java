package demo.pages;

import demo.webdriver.WebPageDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArticlePage {

    //WebDriverWait wait = new WebDriverWait(WebPageDriver.webDriver, 10);

    public String getTitle(){
        WebElement titleArticle = WebPageDriver.webDriver.findElement(By.xpath("//big/b"));
        return titleArticle.getText();

    }
    public String getNumber(){
        WebElement numberPokemon = WebPageDriver.webDriver.findElement(By.xpath("//big/big/a/span"));
        return numberPokemon.getText();
    }
}
