Feature: Search Pokemon

  Scenario: Search Pokemon on wiki
    Given User is on PokemonWiki homepage
    When User input search "Pikachu (Pokémon)" on homepage
    And User press enter on homepage
    Then User see article with title "Pikachu"
    And User see "#025" on article page

  Scenario: Search Pokemon on wiki
    Given User is on PokemonWiki homepage
    When User input search "Bulbasaur (Pokémon)" on homepage
    And User press enter on homepage
    Then User see article with title "Bulbasaur"
    And User see "#001" on article page

  Scenario: Search Pokemon on wiki
    Given User is on PokemonWiki homepage
    When User input search "Snorlax (Pokémon)" on homepage
    And User press enter on homepage
    Then User see article with title "Snorlax"
    And User see "#143" on article page