package demo.steps;

import demo.pages.ArticlePage;
import demo.pages.HomePage;
import demo.webdriver.WebPageDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class PokemonStepDef {

    private final HomePage homePage = new HomePage();
    private final ArticlePage articlePage = new ArticlePage();

    @Given("User is on PokemonWiki homepage")
    public void userIsOnPokemonWikiHomepage() {
        homePage.openHome();
    }

    @When("User input search {string} on homepage")
    public void userInputSearchOnHomepage(String keyword) {
        homePage.search(keyword);
    }

    @When("User press enter on homepage")
    public void userPressEnterOnHomepage() {
        homePage.enterKey();
    }

    @Then("User see article with title {string}")
    public void userSeeArticleWithTitleOnArticlePage(String title) {
        String actual_title = articlePage.getTitle();
        Assert.assertEquals(title, actual_title);
    }

    @And("User see {string} on article page")
    public void userSeeOnArticlePage(String number) {
        String actual_number = articlePage.getNumber();
        Assert.assertEquals(number, actual_number);
    }
}
