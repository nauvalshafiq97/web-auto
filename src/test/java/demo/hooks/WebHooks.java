package demo.hooks;

import demo.webdriver.WebPageDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class WebHooks {

    @Before
    public void initializeWebDriver(){
        WebPageDriver.initialize();
    }

    @After
    public void quitWebDriver(){
        WebPageDriver.quit();
    }
}
